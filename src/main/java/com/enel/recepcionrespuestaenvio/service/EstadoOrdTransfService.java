package com.enel.recepcionrespuestaenvio.service;

public interface EstadoOrdTransfService {

	boolean actualizarEstadoOrden(Integer codEstEnvia, String codigoTipoTdc, String codigoExternoTdc, String codigoInternoTdc, String codigoResultado, String codigoDescripcionResultado, String nombreFichero);

}
