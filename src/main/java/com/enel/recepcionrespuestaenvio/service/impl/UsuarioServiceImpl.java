package com.enel.recepcionrespuestaenvio.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionrespuestaenvio.repository.UsuarioRepository;
import com.enel.recepcionrespuestaenvio.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Override
	public Long obtenerIdUsuario(String user) {
		return usuarioRepository.obtenerIdUsuario(user);
	}

}
