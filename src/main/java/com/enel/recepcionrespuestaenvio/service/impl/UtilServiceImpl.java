package com.enel.recepcionrespuestaenvio.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionrespuestaenvio.repository.UtilRepository;
import com.enel.recepcionrespuestaenvio.service.UtilService;

@Service
public class UtilServiceImpl implements UtilService {

	@Autowired
	private UtilRepository utilRepository;
	
	@Override
	public Date obtenerFechaSistema() {
		return utilRepository.obtenerFechaSistema();
	}
}
