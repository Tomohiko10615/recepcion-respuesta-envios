package com.enel.recepcionrespuestaenvio.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionrespuestaenvio.repository.EstadoOrdTransfDetRepository;
import com.enel.recepcionrespuestaenvio.repository.EstadoOrdTransfRepository;
import com.enel.recepcionrespuestaenvio.service.EstadoOrdTransfService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class EstadoOrdTransfServiceImpl implements EstadoOrdTransfService {

	@Autowired
	private EstadoOrdTransfRepository estadoOrdTransfRepository;
	
	@Autowired
	private EstadoOrdTransfDetRepository estadoOrdTransfDetRepository;

	@Override
	@Transactional
	public boolean actualizarEstadoOrden(Integer codEstEnvia, String codigoTipoTdc, String codigoExternoTdc,
			String codigoInternoTdc, String codigoResultado, String codigoDescripcionResultado, String codigoFichero) {
		Long idOrdTransfer = estadoOrdTransfRepository.obtenerIdOrdTransfer(codigoTipoTdc, codigoExternoTdc);
		
		log.info(codigoTipoTdc);
		log.info(codigoExternoTdc);
		
		if (idOrdTransfer == null) {
			log.error("No se recupero Codigo de Transferencia para {} y {} en {}",
					codigoTipoTdc, codigoExternoTdc, codigoFichero);
			return false;
		}
		
		String observacion = codigoDescripcionResultado.replace("''"," ");
		
		log.info(observacion);
		
		if (codigoInternoTdc == null || codigoInternoTdc.equals("")) {
			codigoInternoTdc = "0";
		}
		
		Integer numCodigoInternoTdc = Integer.parseInt(codigoInternoTdc);
		
		estadoOrdTransfRepository.acutalizarEstadoOrden(
				codEstEnvia, codigoResultado, observacion, numCodigoInternoTdc, idOrdTransfer);
		
		estadoOrdTransfDetRepository.actualizarEstadoOrden(codigoResultado, idOrdTransfer);
		
		return true;
	}
	


}
