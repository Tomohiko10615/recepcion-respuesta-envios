package com.enel.recepcionrespuestaenvio.service;

public interface UsuarioService {

	Long obtenerIdUsuario(String user);

}
