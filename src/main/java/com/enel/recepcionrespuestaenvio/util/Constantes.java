package com.enel.recepcionrespuestaenvio.util;

public class Constantes {

	private Constantes() {
		super();
	}
	public static final String ESQUEMA = "SCHSCOM";
	public static final String ESQUEMADOT = "SCHSCOM.";
	public static final String MSJ_ERROR_RUTA = "No se encuentra el archivo con ruta: {}";
	public static final String PATH_LEGACY = "PATH_LEGACY";
	public static final String PATH_GDI = "PATH_GDI";
	public static final String MSJ_ERROR_ARCHIVO_ERRORES = "No se pudo generar el archivo de salida de Errores {}";
	public static final String LIS_GEST_IMP = "SCR.01 SCR.02 COB.01 COB.02";
	public static final String LIS_GEST_PERD = "INS.01 INS.02 INS.03 INS.04 NOR.01 NOR.02 NOR.03";
	public static final String LIS_CONEX = "NCX.01 NCX.02 NCX.03 NCX.04 NCX.05 NCX.06";
	public static final String LIS_FISCAL = "FIS.01 FIS.02";
	public static final String LIS_DES_COM = "DC.01 DC.02 DC.03";
	
}
