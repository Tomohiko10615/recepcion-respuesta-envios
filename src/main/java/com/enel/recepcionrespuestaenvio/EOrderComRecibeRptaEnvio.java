package com.enel.recepcionrespuestaenvio;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;

import com.enel.recepcionrespuestaenvio.model.Linea;
import com.enel.recepcionrespuestaenvio.service.EstadoOrdTransfService;
import com.enel.recepcionrespuestaenvio.service.ParametrosService;
import com.enel.recepcionrespuestaenvio.service.UsuarioService;
import com.enel.recepcionrespuestaenvio.util.Constantes;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class EOrderComRecibeRptaEnvio {

	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private ParametrosService parametrosService;
	
	@Autowired
	private EstadoOrdTransfService estadoOrdTransfService;
	
	private String user;
	private String sistema;
	private String odt;
	private Date fecha;
	
	private Long idUsuario;
	
	private Integer codEstEnvia;
	private Integer codEstEnver;
	
	private String rutaLegacysIn;
	private String rutaLegacysProc;
	private String rutaLgcGiOu;
	private String rutaLgcGpOu;
	private String rutaLgcNcOu;
	private String rutaLgcDcOu;
	private String rutaLgcFiOu;
	private String rutaGdiInSyn;
	private String rutaGdiInMun;
	private String rutaGdiInMan;
	
	private String[] paths = new String[10];
	
	private String[] entidades = {
			Constantes.PATH_LEGACY, Constantes.PATH_LEGACY,
			Constantes.PATH_LEGACY, Constantes.PATH_LEGACY,
			Constantes.PATH_LEGACY, Constantes.PATH_LEGACY,
			Constantes.PATH_LEGACY, Constantes.PATH_GDI,
			Constantes.PATH_GDI, Constantes.PATH_GDI};
	
	private String[] keys = {
			"LGC_W_IN", "LGC_PROCES", "LGC_GI_OU", "LGC_GP_OU", "LGC_NC_OU",
			"LGC_DC_OU", "LGC_FI_OU", "GDI_IN_SYN", "GDI_IN_MUN", "GDI_IN_MAN"};
	
	private String nombreArchivo;
	private String pathGdiIn;
	
	private String archivoEncontrado;
	
	private String nomArchivoGestImp;
	private String nomArchivoGestPerd;
	private String nomArchivoConex;
	private String nomArchivoDesCom;
	private String nomArchivoFiscal;
	
	private File fArchivoErrGestImp;
	private File fArchivoErrGestPerd;
	private File fArchivoErrConex;
	private File fArchivoErrDesCom;
	private File fArchivoErrFiscal;

	private FileWriter fArchivoErrGestImpWriter;
	private FileWriter fArchivoErrGestPerdWriter;
	private FileWriter fArchivoErrConexWriter;
	private FileWriter fArchivoErrDesComWriter;
	private FileWriter fArchivoErrFiscalWriter;
	
	private Integer nroLinea = 0;
	private Integer filasProc = 0;
	private Integer filasErr  = 0;
	
	private Integer archivosEncontrados = 0;
	
	private Long lineasXArchivo = 0L;
	
	private Linea lineaActual;
	
	public boolean obtenerIdUsuario() {
		idUsuario = usuarioService.obtenerIdUsuario(user);
		if (idUsuario == null) {
			log.error("Error: El usuario No Existe {}", user);
			return false;
		}
		log.info("Se obtuvo la id de usuario: {}", idUsuario);
		return true;
	}

	public boolean obtenerEstadosTransferencia() {
		codEstEnvia = parametrosService.obtenerEstadoTransferencia("ENVIA");
		
		if (codEstEnvia == null) {
			log.error("Error: No se encuentra configurado el Estados de Transferencia [ENVIA]");
			return false;
		}
		
		codEstEnver = parametrosService.obtenerEstadoTransferencia("ENVER");
		
		if (codEstEnver == null) {
			log.error("Error: No se encuentra configurado el Estados de Transferencia [ENVER]");
			return false;
		}
		
		log.info("Se obtuvo el estado de transferencia {}", codEstEnvia);
		log.info("Se obtuvo el estado de transferencia {}", codEstEnver);
		
		return true;
	}

	public boolean obtenerPaths() {
		for (int i = 0; i < 10; i++) {
			paths[i] = parametrosService.obtenerPath(entidades[i], keys[i]);
			if (paths[i] == null) {
				log.error("Error al obtener ruta de archivos {}", keys[i]);
				return false;
			}
			log.info("Se obtuvo la ruta: {}", paths[i]);
		}
		rutaLegacysIn = paths[0];
		rutaLegacysProc = paths[1];
		rutaLgcGiOu = paths[2];
		rutaLgcGpOu = paths[3];
		rutaLgcNcOu = paths[4];
		rutaLgcDcOu = paths[5];
		rutaLgcFiOu = paths[6];
		rutaGdiInSyn = paths[7];
		rutaGdiInMun = paths[8];
		rutaGdiInMan = paths[9];
		
		log.info("Se han obtenido todas las rutas de archivos con éxito.");
		
		return true;
	}

	public boolean armarNombreArchivo() {
		nombreArchivo = parametrosService.armarNombreArchivo(sistema);
		if (nombreArchivo == null) {
			log.error("Error: No se pudo generar el nombre del archivo de respuesta de envío {}", sistema);
			return false;
		}
		
		log.info("Se armó el nombre del archivo: {}", nombreArchivo);

		return true;
	}

	public boolean abrirArchivos(String sistema) {
		
		switch (sistema) {
		case ("EDESYN"):
			if (!abrirArchivosEdesyn()) {
				return false;
			}
			break;
		case ("EDEMUN"):
			if (!abrirArchivosEdemun()) {
				return false;
			}
			break;
		case ("EDEMAN"):
			if (!abrirArchivosEdeman()) {
				return false;
			}
			break;
		default:
			break;
		}

		return true;
	}

	private boolean abrirArchivosEdeman() {
		fArchivoErrFiscal = new File(nomArchivoFiscal);
		try {
			if (!fArchivoErrFiscal.createNewFile()) {
				fArchivoErrFiscalWriter = new FileWriter(fArchivoErrFiscal, false);
			} else {
				fArchivoErrFiscalWriter = new FileWriter(fArchivoErrFiscal);
			}
		} catch (IOException e) {
			e.printStackTrace();
			log.error(Constantes.MSJ_ERROR_ARCHIVO_ERRORES, nomArchivoFiscal);
			return false;
		}
		
		log.info(nomArchivoFiscal);
		
		fArchivoErrGestImp = new File(nomArchivoGestImp);
		try {
			if (!fArchivoErrGestImp.createNewFile()) {
				fArchivoErrGestImpWriter = new FileWriter(fArchivoErrGestImp);
			} else {
				fArchivoErrGestImpWriter = new FileWriter(fArchivoErrGestImp, false);
			}
		} catch (IOException e) {
			e.printStackTrace();
			log.error(Constantes.MSJ_ERROR_ARCHIVO_ERRORES, nomArchivoGestImp);
			return false;
		}
		log.info(nomArchivoGestImp);
		return true;
	}

	private boolean abrirArchivosEdemun() {
		fArchivoErrDesCom = new File(nomArchivoDesCom);
		try {
			if (!fArchivoErrDesCom.createNewFile()) {
				fArchivoErrDesComWriter = new FileWriter(fArchivoErrDesCom, false);
			} else {
				fArchivoErrDesComWriter = new FileWriter(fArchivoErrDesCom);
			}
		} catch (IOException e) {
			e.printStackTrace();
			log.error(Constantes.MSJ_ERROR_ARCHIVO_ERRORES, nomArchivoDesCom);
			return false;
		}
		log.info(nomArchivoDesCom);
		return true;
	}

	private boolean abrirArchivosEdesyn() {
		fArchivoErrGestImp = new File(nomArchivoGestImp);
		try {
			if (!fArchivoErrGestImp.createNewFile()) {
				fArchivoErrGestImpWriter = new FileWriter(fArchivoErrGestImp, false);
			} else {
				fArchivoErrGestImpWriter = new FileWriter(fArchivoErrGestImp);
			}
		} catch (IOException e) {
			e.printStackTrace();
			log.error(Constantes.MSJ_ERROR_ARCHIVO_ERRORES, nomArchivoGestImp);
			return false;
		}
		log.info(nomArchivoGestImp);
		
		fArchivoErrGestPerd = new File(nomArchivoGestPerd);
		try {
			if (!fArchivoErrGestPerd.createNewFile()) {
				fArchivoErrGestPerdWriter = new FileWriter(fArchivoErrGestPerd, false);
			} else {
				fArchivoErrGestPerdWriter = new FileWriter(fArchivoErrGestPerd);
			}
		} catch (IOException e) {
			e.printStackTrace();
			log.error(Constantes.MSJ_ERROR_ARCHIVO_ERRORES, nomArchivoGestPerd);
			return false;
		}
		log.info(nomArchivoGestPerd);
		
		fArchivoErrConex = new File(nomArchivoConex);
		try {
			if (!fArchivoErrConex.createNewFile()) {
				fArchivoErrConexWriter = new FileWriter(fArchivoErrConex, false);
			} else {
				fArchivoErrConexWriter = new FileWriter(fArchivoErrConex);
			}
		} catch (IOException e) {
			e.printStackTrace();
			log.error(Constantes.MSJ_ERROR_ARCHIVO_ERRORES, nomArchivoConex);
			return false;
		}
		
		log.info(nomArchivoConex);
		
		return true;
		
	}

	public boolean procesarArchivos() throws IOException, InterruptedException {
		
		String comando = "for i in `ls " + nombreArchivo + "*` ;do basename $i;done";
		log.info(comando);

		Process process = Runtime.getRuntime().exec(new String[]{"bash","-c",comando});
		process.waitFor();
		
		InputStream inputStream = process.getInputStream();
		
		BufferedReader reader = new BufferedReader(
		         new InputStreamReader(inputStream));
	     
		for (String line; (line = reader.readLine()) != null; ) {  
			
			archivosEncontrados++;
			
			archivoEncontrado = line;
			
			log.info("Archivo: {}", archivoEncontrado);
			
			comando = "mv " + pathGdiIn + archivoEncontrado + " " + rutaLegacysIn;
			
			log.info("Comando: {}", comando);
			
			process = Runtime.getRuntime().exec(new String[]{"bash","-c",comando});
			process.waitFor();
			
			if (process.exitValue() != 0) {
				log.error("No se pudo mover el archivo: {}", comando);
				return false;
			}
			
			log.info("Archivo encontrado: {}", archivoEncontrado);
			
			if (!procesarArchivoEncontrado()) {
				return false;
			}
			
			log.info("Archivo: {} -- Cantidad de lineas: {}", archivoEncontrado, lineasXArchivo);
	    }
		
		if (archivosEncontrados == 0) {
			log.info("No se encontraron archivos para procesar: {}", pathGdiIn);
		}
		
		return true;
	}

	private boolean procesarArchivoEncontrado() throws IOException {
		File fArchivoEncontrado = new File(rutaLegacysIn + archivoEncontrado);
		
		if (!fArchivoEncontrado.exists()) {
			log.error("No se pudo leer el archivo de respuesta {}", rutaLegacysIn + archivoEncontrado);
			return false;
		}
		
		FileReader reader = new FileReader(fArchivoEncontrado);
		try (BufferedReader fArchivoEncontradoReader = new BufferedReader(reader)) {
			lineasXArchivo = 0L;
			
			String line = fArchivoEncontradoReader.readLine();
			
			while (line != null) {
				lineasXArchivo++;
				log.info(lineasXArchivo.toString());
				nroLinea++;
				
				lineaActual = null;
				
				//String col = null;
				//String[] cols = new String[11];
				String[] cols = line.split("\\|", -1);
				
				
				/*
				StringTokenizer sTLine = new StringTokenizer(line,"\r\n\r");
				StringTokenizer sTCol = new StringTokenizer(sTLine.nextToken() + "|","|");
				int colCount = 0;
				*/
				
				log.info("Cargando línea...");
				/*
				while (sTCol.hasMoreTokens() && colCount < 11) {
					col = sTCol.nextToken();
					cols[colCount] = col;
					colCount++;
					log.info(col);
				}
				
				
				if (sTCol.hasMoreTokens() || colCount != 11) {
					log.error("No se pudo cargar la línea {}", nroLinea);
				} else {
					lineaActual =
							new Linea(cols[0], cols[1], cols[2], cols[3], cols[4], cols[5],
									cols[6], cols[7], cols[8], cols[9], cols[10]);
					procesarLinea();
				}
				*/
				
				if (cols.length != 11) {
					log.error("No se pudo cargar la línea {} / {}", lineasXArchivo ,nroLinea);
				} else {
					lineaActual =
							new Linea(cols[0], cols[1], cols[2], cols[3], cols[4], cols[5],
									cols[6], cols[7], cols[8], cols[9], cols[10]);
					procesarLinea();
				}
				
				line = fArchivoEncontradoReader.readLine();
			}
		}
		
		return true;
		
	}

	private void procesarLinea() throws IOException {
		
		if (lineaActual.getCodigoResultado().equals("00")
				|| lineaActual.getCodigoResultado().equals("ODL002")) {
			filasProc++;
			if (!estadoOrdTransfService.actualizarEstadoOrden(
					codEstEnvia,
					lineaActual.getCodigoTipoTdc(),
					lineaActual.getCodigoExternoTdc(),
					lineaActual.getCodigoInternoTdc(),
					lineaActual.getCodigoResultado(),
					lineaActual.getCodigoDescripcionResultado(),
					lineaActual.getNombreFichero())) {
				log.error("No se pudo actualizar el estado de la orden COD_EST_ENVIA");
			}
		} else {
			filasErr++;
			log.error(this.archivoEncontrado);
			if (!estadoOrdTransfService.actualizarEstadoOrden(
					codEstEnver,
					lineaActual.getCodigoTipoTdc(),
					lineaActual.getCodigoExternoTdc(),
					lineaActual.getCodigoInternoTdc(),
					lineaActual.getCodigoResultado(),
					lineaActual.getCodigoDescripcionResultado(),
					lineaActual.getNombreFichero())) {
				log.error("No se pudo actualizar el estado de la orden COD_EST_ENVER");
				escribirError();
			}
		}
		
	}

	private void escribirError() throws IOException {
		StringTokenizer sT = null;
		String outputText = "TIPO DE ORDEN TDC: " + lineaActual.getCodigoTipoTdc()
		+ ", NRO DE ORDEN: " + lineaActual.getCodigoExternoTdc()
		+ ", ERROR: " + lineaActual.getCodigoDescripcionResultado() + "\n";
		
		switch (sistema) {
		case ("EDESYN"):
			sT = new StringTokenizer(Constantes.LIS_GEST_IMP, lineaActual.getCodigoTipoTdc());
			if (sT.hasMoreTokens()) {
				fArchivoErrGestImpWriter.write(outputText);
			}
			
			sT = new StringTokenizer(Constantes.LIS_GEST_PERD, lineaActual.getCodigoTipoTdc());
			if (sT.hasMoreTokens()) {
				fArchivoErrGestPerdWriter.write(outputText);
			}
			
			sT = new StringTokenizer(Constantes.LIS_CONEX, lineaActual.getCodigoTipoTdc());
			if (sT.hasMoreTokens()) {
				fArchivoErrConexWriter.write(outputText);
			}
			
			break;
		case ("EDEMUN"):
			fArchivoErrDesComWriter.write(outputText);
			fArchivoErrDesComWriter.close();
			break;
		case ("EDEMAN"):
			sT = new StringTokenizer(Constantes.LIS_FISCAL, lineaActual.getCodigoTipoTdc());
			if (sT.hasMoreTokens()) {
				fArchivoErrFiscalWriter.write(outputText);
			}
			
			sT = new StringTokenizer(Constantes.LIS_GEST_IMP, lineaActual.getCodigoTipoTdc());
			if (sT.hasMoreTokens()) {
				fArchivoErrGestImpWriter.write(outputText);
			}
			fArchivoErrFiscalWriter.close();
			fArchivoErrGestImpWriter.close();
			break;
		default:
			break;
		}
	}

	public void cerrarArchivos(String sistema) throws IOException {
		switch (sistema) {
		case ("EDESYN"):
			fArchivoErrConexWriter.close();
			fArchivoErrGestImpWriter.close();
			fArchivoErrGestPerdWriter.close();
			break;
		case ("EDEMUN"):
			fArchivoErrDesComWriter.close();
			break;
		case ("EDEMAN"):
			fArchivoErrFiscalWriter.close();
			fArchivoErrGestImpWriter.close();
			break;
		default:
			break;
		}
		
	}
	
}
