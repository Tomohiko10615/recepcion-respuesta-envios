package com.enel.recepcionrespuestaenvio.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.recepcionrespuestaenvio.entity.EstadoOrdTransf;
import com.enel.recepcionrespuestaenvio.util.Constantes;

@Repository
public interface EstadoOrdTransfRepository extends JpaRepository<EstadoOrdTransf, Long>{

	@Query(value="SELECT DISTINCT id_ord_transfer "
			+ "FROM " + Constantes.ESQUEMADOT + "eor_ord_transfer "
			+ "WHERE cod_tipo_orden_eorder = ?1 "
			+ "AND nro_orden_legacy = ?2 "
			+ "AND cod_estado_orden IN (2,4)", nativeQuery=true)
	Long obtenerIdOrdTransfer(String codigoTipoTdc, String codigoExternoTdc);

	@Modifying
	@Transactional
	@Query(value="UPDATE " + Constantes.ESQUEMADOT + "EOR_ORD_TRANSFER SET "
			+ "COD_ESTADO_ORDEN_ANT = COD_ESTADO_ORDEN, "
			+ "COD_ESTADO_ORDEN = ?1, "
			+ "COD_OPERACION = ?2, "
			+ "OBSERVACIONES = ?3, "
			+ "NRO_ORDEN_EORDER = ?4, "
			+ "FEC_ESTADO = now() "
			+ "WHERE id_ord_transfer = ?5", nativeQuery=true)
	void acutalizarEstadoOrden( Integer codEstEnvia, String codigoResultado,
			String observacion, Integer numCodigoInternoTdc, Long idOrdTransfer);
	
}
