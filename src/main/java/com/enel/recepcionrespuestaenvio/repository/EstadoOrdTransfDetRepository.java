package com.enel.recepcionrespuestaenvio.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.recepcionrespuestaenvio.entity.EstadoOrdTransfDet;
import com.enel.recepcionrespuestaenvio.util.Constantes;

@Repository
public interface EstadoOrdTransfDetRepository extends JpaRepository<EstadoOrdTransfDet, Long> {

	@Modifying
	@Transactional
	@Query(value="UPDATE " + Constantes.ESQUEMADOT + "eor_ord_transfer_det "
			+ "SET cod_error = ?1 "
			+ "WHERE id_ord_transfer = ?2", nativeQuery=true)
	void actualizarEstadoOrden(String codigoResultado, Long idOrdTransfer);

}
