package com.enel.recepcionrespuestaenvio.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "eor_ord_transfer")
public class EstadoOrdTransf {

	@Id
	@Column(name = "id_order_transfer")
	private Long idOrderTransfer;
	
	@Column(name = "cod_tipo_orden_eorder")
	private String codTipoOrdenEorder;
	
	@Column(name = "nro_orden_legacy")
	private String nroOrdenLegacy;
	
	@Column(name = "cod_estado_orden")
	private Integer codEstadoOrden;
	
	@Column(name = "cod_estado_orden_ant")
	private Integer codEstadoOrdenAnt;
	
	@Column(name = "cod_operacion")
	private String codOperacion;
	
	private String observaciones;
	
	@Column(name = "nro_orden_eorder")
	private Integer nroOrdenEorder;
	
	@Column(name = "fec_estado")
	private Date fecEstado;
}
