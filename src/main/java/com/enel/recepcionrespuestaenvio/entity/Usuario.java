package com.enel.recepcionrespuestaenvio.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.enel.recepcionrespuestaenvio.util.Constantes;

import lombok.Data;

@Entity
@Data
@Table(name = "usuario", schema = Constantes.ESQUEMA)
public class Usuario {

	@Id
	private Long id;
	
	private String username;
}
