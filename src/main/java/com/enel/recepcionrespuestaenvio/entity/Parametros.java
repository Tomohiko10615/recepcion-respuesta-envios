package com.enel.recepcionrespuestaenvio.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.enel.recepcionrespuestaenvio.util.Constantes;

import lombok.Data;

@Entity
@Data
@Table(name = "com_parametros", schema = Constantes.ESQUEMA)
public class Parametros {

	@Id
	private Long id;
	
	@Column(name = "valor_num")
	private String valorNum;
	
	private String sistema;
	private String entidad;
	private String codigo;
	
}
