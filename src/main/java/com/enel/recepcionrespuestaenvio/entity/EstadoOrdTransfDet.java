package com.enel.recepcionrespuestaenvio.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "estado_ord_transf_det")
public class EstadoOrdTransfDet {

	@Id
	@Column(name = "id_ord_transfer_det")
	private Long idOrdTransferDet;
	
	@Column(name = "cod_error")
	private String codError;
	
	@Column(name = "id_ord_transfer")
	private String idOrdTransfer;
	
	@Column(name = "cod_estado_orden")
	private Integer codEstadoOrden;
}
