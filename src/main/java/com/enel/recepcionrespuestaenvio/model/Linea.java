package com.enel.recepcionrespuestaenvio.model;

import org.springframework.data.repository.NoRepositoryBean;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@NoRepositoryBean
public class Linea {

	private String nombreFichero;
	private String llaveSecreta;
	private String codigoDistribuidora;
	private String codigoSistemaExternoOrigen;
	private String codigoTipoTdc;
	private String codigoExternoTdc;
	private String codigoProceso;
	private String codigoSubProceso;
	private String codigoInternoTdc;
	private String codigoResultado;
	private String codigoDescripcionResultado;
	
}
